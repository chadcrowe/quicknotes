from django.db import models
from tkinter import ttk
from django.utils import timezone
import django
#On creation of checklist, create widget instances for each NoteEntry
class NoteEntry(models.Model):
    noteText = models.CharField(max_length=1000)
    pubDate = models.DateTimeField('date published',default = django.utils.timezone.now)
    stickyNoteParent = models.ForeignKey('StickyNote.StickyNoteInstance',null = True)


