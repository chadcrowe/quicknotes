from django.db import models
from django.utils import timezone
import sys
import django
from PyQt5.QtWidgets import QApplication
import uuid
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from Checklist.models import NoteEntry
from PyQt5.QtGui import QColor
from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QMenu
import django
import time

class OldStickyNoteInstance(models.Model):
    #hidden = models.BooleanField(default = False)
    pubDate = models.DateTimeField('date published',default = django.utils.timezone.now)
    unique_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    height = models.FloatField(default = 400)
    width = models.FloatField(default = 400)
    screenHeight = models.FloatField(default = 400)
    screenWidth = models.FloatField(default = 400)
    color = models.IntegerField(default = 3,blank = False,null = False)
    def assignWidget(self,text):
        self.widget = StickyNote(self,text)
        
class OldStickyNote(QWidget):
    def __init__(self,snInstance,text):
        super().__init__()
        self.initUI()
        self.setOldColor(snInstance)
        self.setText(text)
    def setOldColor(self,snInstance):
        colorObj = self.getColor(snInstance.color)
        self.setColor(colorObj)
    def setText(self,text):
        self.notepad.setPlainText("")
        self.notepad.append(str(text))
        try:
            title = text.split("\n")[0]
            self.setWindowTitle(title)
        except:
            self.setWindowTitle('Note')
    def initUI(self):
        self.setGeometry(400,400,400,400)
        self.setMinimumSize(10, 10)
        self.setMaximumSize(5000, 5000)
        self.notepad = QTextEdit(self)
        self.nextOldNote = QPushButton("Next Note",self)
        self.nextOldNote.clicked.connect(self.getNextOldNote) 
        font = self.notepad.font()
        font.setFamily("Courier")
        self.notepad.setFontPointSize(12)
        layout = QGridLayout()
        layout.addWidget(self.notepad,1,0,1,1)
        layout.addWidget(self.nextOldNote,0,0)
        layout.setColumnStretch(0,7)
        layout.setAlignment(Qt.AlignCenter)
        self.setLayout(layout)
        #self.snInstance.color = int(colorObj)
        self.notepad.setFocus()
    def getNextOldNote(self):
        self.mainRef.showNextOldNote()
    def setColor(self,qcolor):
        p = self.palette()
        colorObj = qcolor
        p.setColor(self.backgroundRole(), colorObj)
        self.setPalette(p)
    def getNextColor(self):
        print("self color: ",self.snInstance.color,type(self.snInstance.color))
        if self.snInstance.color == 3:
            self.snInstance.color = 7
        elif self.snInstance.color == 7:
            self.snInstance.color = 8
        elif self.snInstance.color == 8:
            self.snInstance.color = 9
        elif self.snInstance.color == 9:
            self.snInstance.color = 11
        elif self.snInstance.color == 11:
            self.snInstance.color = 12
        elif self.snInstance.color == 12:
            self.snInstance.color = 19
        elif self.snInstance.color == 19:
            self.snInstance.color = 3
        colorObj = self.getColor(self.snInstance.color)
        self.setColor(colorObj)
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.close()
            self.mainRef.close()
    def getColor(self,colorVal):
        self.colorDict = {"3":Qt.white,"7":Qt.red,"8":Qt.green,"9":Qt.blue,"11":Qt.magenta,"12":Qt.yellow,"19":Qt.transparent}
        try:
            if self.colorDict[str(colorVal)]:
                return self.colorDict[str(colorVal)]
            else:
                return Qt.white
        except:
            return Qt.white
    def giveMainRef(self,mainRef):
        self.mainRef = mainRef
    def contextMenuEvent(self, event):
        menu = QMenu(self)
        menu.addAction("Color")
    def buttonClicked(self):
        pass
