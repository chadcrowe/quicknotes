from django.apps import AppConfig


class OldstickynoteConfig(AppConfig):
    name = 'OldStickyNote'
