print("Running QuickNotes!")
import PyQt5
import sqlite3 as lite
import datetime
import time
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
import win32gui
import pywinauto
import win32con
import win32api
import os
import django
django.setup()
from Checklist.models import NoteEntry
from StickyNote.models import StickyNoteInstance, StickyNote
from QuickNotes import stopwatch
from django.utils import timezone
from QuickNotes.stopwatch import Stopwatch 
from os.path import expanduser
from OldStickyNote.models import *
import signal
user_folder = expanduser("~")
QuickNotes_folder = os.path.join(user_folder,"QuickNotes")
records_db_location = os.path.join(QuickNotes_folder,"records.db")
#Classes
class TimeRecorder:
    def __init__(self,startTime):
        self.startTime = startTime
class Window():
    def recordWorkSQL(self,projectText, totalTime,startTime,endTime):
        conn = lite.connect(records_db_location)
        c = conn.cursor()
        c.execute("INSERT INTO workTimer (project, totalTime, startTime, endTime) VALUES (?,?,?,?)",(projectText, totalTime, startTime, endTime))
        conn.commit()
        conn.close()
    def enterSQL(self,text):
        if text:
            conn = lite.connect(records_db_location)
            c = conn.cursor()
            date = datetime.datetime.now()
            c.execute("INSERT INTO infoTable (description, month, day, year, hour, minute, second) VALUES (?,?,?,?,?,?,?)",(text,date.month,date.day,date.year,date.hour,date.minute,date.second))
            conn.commit()
            conn.close()
    def __init__(self):
        self.StickyNoteApplicationsList = []
        self.colors = [3,7,8,9,11,12,5]
        self.colorsCnt = 0
        self.app = QApplication(sys.argv)
        self.showStickyNotes(False)
        self.app.exec_()
        #self.app.quit()
    def close(self):
        for SN in self.StickyNoteApplicationsList:
            SN.widget.close()
        #signal.signal(signal.SIGINT, signal.SIG_DFL)
        self.app.deleteLater()
    def StartProject(self):
        self.StartProjectButton.config(text = "Finish Work")
        self.stopwatch.restart()
        #self.update_timer_label()
        self.stopwatch.start_timer()
        startTimeInstance.startTime = self.stopwatch.start_time#This will call a function outside
            #the class, to store a value outside this window instance
            #for use when tkinter creates a new note-taker instance
        self.StartProjectButton.config(command = self.FinishProject)
    def FinishProject(self):
        self.stopwatch.end_timer()
        self.StartProjectButton.config(text = "Start Work")
        recorded_start_datetime = datetime.datetime.fromtimestamp(self.stopwatch.start_time).strftime('%Y-%m-%d %H:%M:%S')
        recorded_end_datetime = datetime.datetime.fromtimestamp(self.stopwatch.end_time).strftime('%Y-%m-%d %H:%M:%S')
        self.recordWorkSQL("No Project Set yet",self.stopwatch.timer_value.get(),recorded_start_datetime,recorded_end_datetime)
        self.StartProjectButton.config(command = self.StartProject)
        startTimeInstance.startTime = 0 #Restarts Timer
    def showStickyNotes(self,showAll = False):
        for SN in self.StickyNoteApplicationsList:
            SN.widget.close()
        self.StickyNoteApplicationsList = []
        num_notes = len(StickyNoteInstance.objects.all())
        if num_notes == 0:
            self.createStickyNoteInstance() #If there are no notes, create one
        allHidden = True
        for sticky in StickyNoteInstance.objects.all():
            print("Sticky hidden ",sticky.hidden)
            if sticky.hidden == True and showAll == False:
                pass #Do not create the note for showing
            else:
                allHidden = False
                notes = sticky.noteentry_set.all()
                text = ""
                for note in notes:
                    text = text + note.noteText
                sticky.assignWidget(text,self)
                sticky.widget.show()
                self.StickyNoteApplicationsList.append(sticky)
        if allHidden == True:
            self.createStickyNoteInstance()#create a new sticky note
                                           #If all others are hidden
    def showOldStickyNotes(self):
        self.OldStickyNoteApplicationsList = []
        num_notes = len(OldStickyNoteInstance.objects.all())
        for sticky in OldStickyNoteInstance.objects.all().order_by('-pubDate'):
            notes = sticky.oldnoteentry_set.all()
            text = ""
            for note in notes:
                text = text + note.noteText
            if text == "":
                sticky.delete()
            else:
                #sticky.assignWidget(text)
                #sticky.widget.show()
                #sticky.widget.giveMainRef(self)
                self.OldStickyNoteApplicationsList.append([sticky,text])
        self.MainOldNoteCnt = 0
        self.MainOldNote = OldStickyNote(self.OldStickyNoteApplicationsList[self.MainOldNoteCnt][0],self.OldStickyNoteApplicationsList[self.MainOldNoteCnt][1])
        self.MainOldNote.show()
        self.MainOldNote.giveMainRef(self)
    def showNextOldNote(self):
        try:
            self.MainOldNoteCnt += 1
            self.MainOldNote.setText(self.OldStickyNoteApplicationsList[self.MainOldNoteCnt][1])
            self.MainOldNote.setOldColor(self.OldStickyNoteApplicationsList[self.MainOldNoteCnt][0])
        except:
            self.MainOldNoteCnt = 0
            self.showNextOldNote()
    def createStickyNoteInstance(self):
        if self.colorsCnt > len(self.colors)-1:
            self.colorsCnt = 0
        sticky = StickyNoteInstance(hidden = False,color = self.colors[self.colorsCnt])
        self.colorsCnt += 1
        sticky.assignWidget("",self)
        sticky.widget.show()
        self.StickyNoteApplicationsList.append(sticky)

                
#Global methods
def getfocus():
    try:
        handle = win32gui.FindWindow(0, "QuickNotes")
        print(handle)
        # use the window handle to set focus
        if handle != 0:
            try:
                win32gui.SetForegroundWindow(handle)
            except:
                try:
                    win32gui.SetForegroundWindow(handle)
                except:
                    print("Could not bring to foreground")
            window = app.window_(handle=handle)
            window.SetFocus()
            print("Focused!")
        else:
            print("Handle is zero")
    except Exception as e:
        print(e)

def enum_callback(hwnd, results):
    winlist.append((hwnd, win32gui.GetWindowText(hwnd)))



#Start
startTimeInstance = TimeRecorder(0)
try:
    os.mkdir(QuickNotes_folder)
except:
    pass
toplist = []
winlist = []
app = pywinauto.application.Application()
try: #Just in case I take it to a new computer and make a new db
    conn = lite.connect(records_db_location)
    c = conn.cursor()
    c.execute('CREATE TABLE infoTable (ID int PRIMARY KEY,description text NOT NULL, month int, day int,year int, hour int, minute int, second int)')
    conn.commit()
    conn.close()
except:
    pass
try: #Just in case I take it to a new computer and make a new db
    conn = lite.connect(records_db_location)
    c = conn.cursor()
    c.execute('CREATE TABLE workTimer (ID int PRIMARY KEY,project text, totalTime REAL, startTime TEXT NOT NULL, endTime TEXT NOT NULL)') #Note that project text can be NULL
    conn.commit()
    conn.close()
except:
    pass

while 1 == 1: 
    while 1==1:
            if not (win32api.GetAsyncKeyState(win32con.VK_F7)):
                    pass
            else:
                break;
    print("running")
    window = Window()
    print("Application has quit")


    


    
