import time
from tkinter import *
from tkinter import ttk
import datetime
class Stopwatch():
    def __init__(self,root,label,timer_value):#I pass timer value for remembering
            #the time value between created tkinter instances
        self.root = root
        self.timer_value = StringVar()
        label.config(textvariable = self.timer_value)
        if timer_value == 0:
            self.ongoing = False
            self.timer_value.set("0:00:00") 
            self.start_time = 0
            self.end_time = 0
            self.on_timer = 0
        else:
            self.ongoing = True
            self.start_time = timer_value
            self.updater = self.root.after(40, self.update_timer)
    def start_timer(self):
        self.start_time = time.time()
        print("starting timer")
        self.update_timer()
    def end_timer(self):
        self.ongoing = False
        self.end_time = time.time()
        self.root.after_cancel(self.updater)
        print("End timer!")
    def display_time(self):
        if self.start_time == 0:
            return 0
        if self.start_time >= self.end_time:
            return time.time() - self.start_time
        elif self.start_time < self.end_time:
            return self.end_time - self.start_time
    def update_timer(self):
        self.updater = self.root.after(40, self.update_timer)
        total_seconds = time.time() - self.start_time
        self.time_formatted = time.strftime("%H:%M:%S", time.gmtime(total_seconds))
        #self.time_formatted = str(datetime.timedelta(seconds=total_seconds))
        self.timer_value.set(self.time_formatted)
    def restart(self):
        self.on_timer = 0
        self.start_time = 0
        self.end_time = 0
        self.timer_value.set("0:00:00")
