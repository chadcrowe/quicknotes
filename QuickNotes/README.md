# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Application to record work related notes and time worked
* 1.0
* 

### How do I get set up? ###

* The .exe is the entire program.  Insert QuickNotes.exe into "C:\Users\UserNameHere\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup".  Windows automatically runs all files in that folder.  By place QuickNotes.exe in the windows Startup folder, Windows will start running the program when it starts up.  To show the app, press F7!!!!  F7 is your magical key to bring the app onto your desktop.  The app will try to bring itself to the front and give itself the screen's focus.  Note, the app is always running in the background (small enough of an app :).

The program itself will create an sqlite database and store it in the folder, "C:\Users\ccrowe\QuickNotes".  The database is called "records.db".  The program will store all of your information here.  Deleting this database will wipe away any data in the program.  The program will re-create this database on the next run

 The python files are only there to read what the software does
* Everything is already configured for you
* The application only works on windows
* The application runs with sqlite for user convenience, so there is no need to setup any databases

### Who do I talk to? ###

* Email chad.crowe@hdrinc.com for any questions or concerns.