from django.db import models
from django.utils import timezone
import sys
import django
from PyQt5.QtWidgets import QApplication
import uuid
import datetime
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from Checklist.models import NoteEntry
from OldChecklist.models import OldNoteEntry
from PyQt5.QtGui import QColor
from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QMenu
from PyQt5 import QtCore
import time
import os
import ntpath
import webbrowser
import re
import pytz
from OldStickyNote.models import OldStickyNoteInstance, OldStickyNote
from generalNote.models import generalNote
from os.path import expanduser
import sqlite3 as lite
import django
from bs4 import BeautifulSoup as BS
class StickyNoteInstance(models.Model):
    hidden = models.BooleanField(default = False)
    pubDate = models.DateTimeField('date published',default = django.utils.timezone.now)
    unique_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    height = models.FloatField(default = 400)
    width = models.FloatField(default = 400)
    screenHeight = models.FloatField(default = 400)
    screenWidth = models.FloatField(default = 400)
    color = models.IntegerField(default = 3,blank = False,null = False)
    fontSize = models.IntegerField(default = 14,blank = False,null = False)
    updated = models.DateTimeField(auto_now=True)
    def assignWidget(self,text,mainRef):
        self.widget = StickyNote(self,text,mainRef)


class StickyNote(QWidget):
    def __init__(self,snInstance,text,mainRef):
        super().__init__()
        self.initUI(snInstance,text,mainRef)
    def initUI(self,snInstance,text,mainRef):
        self.mainRef = mainRef
        self.initText = text
        colorObj = self.getColor(snInstance.color)
        self.setColor(colorObj)
        self.setGeometry(snInstance.screenWidth,snInstance.screenHeight,float(snInstance.width),float(snInstance.height))
        self.setMinimumSize(10, 10)
        self.setMaximumSize(5000, 5000)
        #SpinBox
        self.sb = QSpinBox()
        self.sb.setMinimum(1)
        self.sb.setMaximum(300)
        self.sb.setValue(snInstance.fontSize)
        #Widgets
        self.numberings = QTextEdit(self)
        self.numberings.setAlignment(Qt.AlignRight)
        self.isHidden = QCheckBox(self)
        self.hiddenLabel = QLabel(self)
        self.hiddenLabel.setText("  Hide  ")
        self.questionButton = QPushButton("About",self)
        self.questionButton.clicked.connect(self.showInfo) 
        self.isHidden.stateChanged.connect(self.checkEvent)
        self.notepad = QTextEdit(self)
        self.inputLine = QLineEdit(self)
        self.inputLine.setFocus()
        hideButton = QPushButton("Delete Note",self)
        changeColor = QPushButton("Next Color",self)
        strikeButton = QPushButton("StrikeOut",self)
        strikeButton.clicked.connect(self.strikeThroughText) 
        changeColor.clicked.connect(self.getNextColor) 
        hideButton.clicked.connect(self.buttonClicked) 
        font = self.notepad.font()
        #font.setFamily("Courier")
        #self.notepad.setPointSize(14)
        #self.numberings.setPointSize(14)
        layout = QGridLayout()
        self.snInstance = snInstance
        text = re.sub(r"""font-size:\d+px;""","font-size:" + str(self.snInstance.fontSize) + "px;",text)#Replace text with new font size
        self.notepad.setStyleSheet("font-size: " + str(self.snInstance.fontSize) + "px;")
        self.numberings.setStyleSheet("* {font-size:" + str(self.snInstance.fontSize) + """px;
font-weight:bold;
margin-right:0;
}
                  """)
        self.notepad.append(str(text))
        self.numberings.setLineWrapMode(QTextEdit.NoWrap)
        #QTextEdit Widgets
        layout.addWidget(self.notepad,1,1,2,7)
        layout.addWidget(self.numberings,1,0,1,1)
        layout.addWidget(self.inputLine,6,0,2,7)
        #Other Widgets
        layout.addWidget(self.isHidden,0,6)
        layout.addWidget(strikeButton,0,3)
        layout.addWidget(self.hiddenLabel,0,5)
        layout.addWidget(changeColor,0,2)
        layout.addWidget(hideButton,0,1)
        layout.addWidget(self.sb,0,4)
        layout.addWidget(self.questionButton,0,0)
        layout.setColumnStretch(0,1)
        layout.setColumnStretch(1,10)
        layout.setAlignment(Qt.AlignCenter)
        layout.setColumnStretch(1,10)
        layout.setColumnMinimumWidth(0,10)


        self.numberings.setMinimumSize(20,10)
        self.numberings.setMaximumSize(60,1000)
        
        self.questionButton.setMaximumSize(60,1000)

        self.setLayout(layout)
        self.snInstance.color = int(colorObj)
        self.uuid = self.snInstance.unique_id
        try:
            self.title = self.notepad.toPlainText().split("\n")[0]
            self.setWindowTitle(self.title)
        except:
            self.setWindowTitle('Note')
        shortcut = QShortcut(self)
        shortcut.setKey("Ctrl+P")
        shortcut.activated.connect(self.file_save)
        
        #Create numberings
        numberingTxt = ""
        self.fill_number_box()
        self.sb.valueChanged.connect(self.spin_box_change)
        #self.notepad.setFocus()
        shortcut2 = QShortcut(self)
        shortcut2.setKey("Ctrl+g")
        shortcut2.activated.connect(self.showGeneralNotes)

        shortcut3 = QShortcut(self)
        shortcut3.setKey("Ctrl+n")
        shortcut3.activated.connect(self.mainRef.createStickyNoteInstance)

        shortcut4 = QShortcut(self)
        shortcut4.setKey("Ctrl+u")
        shortcut4.activated.connect(lambda: self.mainRef.showStickyNotes(True))

        print(self.numberings.toHtml())
    def showInfo(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("QuickNotes created by Chad Crowe")
        msg.setInformativeText("Shortcuts:\nCtrl+n: Open a new sticky note\nEsc: close all sticky notes\nCtrl+u: Unhide all stikcy notes\nCtrl+p: Print sticky note\nCtrl+g print all general notes")
        msg.setWindowTitle("QuckNotes About")
        msg.setDetailedText("The details are as follows:\nThe first line in the sticky note is its title.\nYou may hide notes by checking the Hide button. Unhide all notes by pressing Ctrl+u.\nThe bottom box records general notes concerning this sticky note. The printer will organize the general notes by sticky note.\nThe left box show the total number of lines in your sticky note")
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        retval = msg.exec_()
    def fill_number_box(self):
        qtDoc = self.notepad.document()
        self.numberings.insertHtml("""<p>""")
        for lineNumber in range(qtDoc.blockCount()-1):
            #if lineNumber == 0:
            #    self.numberings.insertHtml("Title")
            if lineNumber < 9:
                self.numberings.insertHtml("""<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;""" + str(lineNumber+1) + ".")#has extra spaces
            else:
                self.numberings.insertHtml("""<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;""" + str(lineNumber+1) + ".")
        self.numberings.insertHtml("</p>")
    def redraw_text(self):
        raw_text = self.notepad.toPlainText()
        html_text = self.notepad.toHtml()
        self.notepad.clear()
        self.numberings.clear()
        text = re.sub(r"""font-size:\d+px;""","font-size:" + str(self.snInstance.fontSize) + "px;",html_text)#Replace text with new font size
        self.notepad.setStyleSheet("font-size: " + str(self.snInstance.fontSize) + "px;")
        self.numberings.setStyleSheet("font-size: " + str(self.snInstance.fontSize) + "px;")
        self.notepad.append(str(text))
        self.fill_number_box()
    def spin_box_change(self):
        self.snInstance.fontSize = self.sb.value()
        self.snInstance.save()
        self.redraw_text()
    def strikeThroughText(self):
        self.notepad.moveCursor(19,1)
        cursor = self.notepad.textCursor()
        format = cursor.charFormat()
        isStruckOut = format.fontStrikeOut()
        self.notepad.moveCursor(9,1)
        cursor = self.notepad.textCursor()
        format = cursor.charFormat()
        if isStruckOut:
            format.setFontStrikeOut(False)
        else:
            format.setFontStrikeOut(True)
        cursor.setCharFormat(format)
        self.strikeThroughNumberings() #Will only strikethrough if I have highlighted something
    def strikeThroughNumberings(self):
        self.numberings.moveCursor(19,1)
        cursor = self.numberings.textCursor()
        format = cursor.charFormat()
        isStruckOut = format.fontStrikeOut()
        self.numberings.moveCursor(9,1)
        cursor = self.numberings.textCursor()
        format = cursor.charFormat()
        if isStruckOut:
            format.setFontStrikeOut(False)
        else:
            format.setFontStrikeOut(True)
        cursor.setCharFormat(format)
    def setColor(self,qcolor):
        p = self.palette()
        colorObj = qcolor
        p.setColor(self.backgroundRole(), colorObj)
        self.setPalette(p)
    def getNextColor(self):
        print("self color: ",self.snInstance.color,type(self.snInstance.color))
        if self.snInstance.color == 3:
            self.snInstance.color = 2
        elif self.snInstance.color == 2:
            self.snInstance.color = 7
        elif self.snInstance.color == 7:
            self.snInstance.color = 13
        elif self.snInstance.color == 13:
            self.snInstance.color = 8
        elif self.snInstance.color == 8:
            self.snInstance.color = 14
        elif self.snInstance.color == 14:
            self.snInstance.color = 9
        elif self.snInstance.color == 9:
            self.snInstance.color = 15
        elif self.snInstance.color == 15:
            self.snInstance.color = 10
        elif self.snInstance.color == 10:
            self.snInstance.color = 16
        elif self.snInstance.color == 16:
            self.snInstance.color = 11
        elif self.snInstance.color == 11:
            self.snInstance.color = 17
        elif self.snInstance.color == 17:
            self.snInstance.color = 12
        elif self.snInstance.color == 12:
            self.snInstance.color = 18
        elif self.snInstance.color == 18:
            self.snInstance.color = 4
        elif self.snInstance.color == 4:
            self.snInstance.color = 6
        elif self.snInstance.color == 6:
            self.snInstance.color = 3
            
            
        colorObj = self.getColor(self.snInstance.color)
        self.setColor(colorObj)
    def checkEvent(self,checked):
        if checked == 2:
            self.close()
        else:
            pass #Making this visible
    def getColor(self,colorVal):
        self.colorDict = {"2":Qt.black,"3":Qt.white,"5":Qt.darkGray,"4":Qt.darkGray,"6":Qt.lightGray,"7":Qt.red,"8":Qt.green,"9":Qt.blue,"11":Qt.magenta,"12":Qt.yellow,"13":Qt.darkRed,"14":Qt.darkGreen,"15":Qt.darkBlue,"10":Qt.cyan,"16":Qt.darkCyan,"17":Qt.darkMagenta,"18":Qt.darkYellow,"19":Qt.transparent}
        try:
            if self.colorDict[str(colorVal)]:
                return self.colorDict[str(colorVal)]
            else:
                return Qt.white
        except:
            return Qt.white
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.mainRef.close()
        if e.key() == QtCore.Qt.Key_Return:
            if self.inputLine.hasFocus():
                self.enterGeneralNote(self.inputLine.text())
                self.inputLine.clear()
    def giveMainRef(self,mainRef):
        self.mainRef = mainRef
    def contextMenuEvent(self, event):
        menu = QMenu(self)
        menu.addAction("Color")
    def closeEvent(self,event):
        #Create old note
        try:
            raw_text = self.notepad.toPlainText()
            html_text = self.notepad.toHtml()
            self.color = self.snInstance.color
            if raw_text != "":
                Oldsticky = OldStickyNoteInstance.objects.create()
                try:
                    exists = OldNoteEntry.objects.filter(noteText = html_text)
                    if len(exists) == 0:
                        n = OldNoteEntry.objects.create(noteText = html_text,stickyNoteParent = Oldsticky)
                        Oldsticky.color = self.color
                        n.save()
                        Oldsticky.save()
                except:
                    print("Note already existed in OldNoteEntries")
        except:
            print("Error saving")
        #Save Note
        try:
            raw_text = self.notepad.toPlainText()
            html_text = self.notepad.toHtml()
            fontSize = self.snInstance.fontSize
            self.color = self.snInstance.color
            self.snInstance.delete()
            if raw_text != "":
                sticky = StickyNoteInstance.objects.create(hidden = True,fontSize = fontSize)
                if self.isHidden.isChecked() == True:
                    sticky.hidden = True #Will need to delete
                else:
                    sticky.hidden = False
                sticky.height = self.size().height()
                sticky.width = self.size().width()
                sticky.screenHeight = self.pos().y()+30
                sticky.screenWidth = self.pos().x()+8
                n = NoteEntry.objects.create(noteText = html_text,stickyNoteParent = sticky)
                sticky.color = self.color
                n.save()
                sticky.save()
        except:
            print("Error saving")
        event.accept()
    def buttonClicked(self):
        instance = StickyNoteInstance.objects.get(unique_id = self.uuid)
        instance.delete()
        try:
            raw_text = self.notepad.toPlainText()
            html_text = self.notepad.toHtml()
            self.color = self.snInstance.color
            if raw_text != "":
                Oldsticky = OldStickyNoteInstance.objects.create()
                try:
                    exists = OldNoteEntry.objects.filter(noteText = html_text)
                    if len(exists) == 0:
                        n = OldNoteEntry.objects.create(noteText = html_text,stickyNoteParent = Oldsticky)
                        Oldsticky.color = self.color
                        n.save()
                        Oldsticky.save()
                except:
                    print("Note already existed in OldNoteEntries")
            else:
                print("note to be deleted")
        except:
            print("Error saving")
        self.notepad.setPlainText("")
        self.close()
    def file_save(self):
        print("Called save file function!")
        name = os.path.dirname(os.path.realpath(__file__)) + "\\" + self.title + ".html"
        file = open(name,'w')
        text = self.notepad.toHtml()
        title = self.notepad.toPlainText().split("\n")[0]
        lines =  text.split("<p")
        file.writelines("<h1>" + title + """</h1><div id="list">
                                                  <ol>""")
        
        for line in lines[2:]:
            file.writelines("<li><p" + line + "</li>")
        file.writelines("""
</ol>
</div>
<style>

h2 {
  font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
  margin: 0;
  padding: 0;
}
 
ol {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
 
li {
  font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
  border-bottom: 1px solid #ccc;
}
 
li:last-child {
  border: none;
}
 
li a {
  text-decoration: none;
  color: #000;
  display: block;
  width: 200px;
 
  -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
  -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
  -o-transition: font-size 0.3s ease, background-color 0.3s ease;
  -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
  transition: font-size 0.3s ease, background-color 0.3s ease;
}
 h1 {font-family: 'Helvetica Neue', sans-serif; font-size: 46px; font-weight: 100; line-height: 50px; letter-spacing: 1px; padding: 0 0 10px; border-bottom: double #555; }
h1 { color: #333333; font-family: 'Bitter', serif; font-size: 50px; font-weight: normal; line-height: 54px;  }
    li {  font-family: 'Verdana', sans-serif; font-size: 16px; line-height: 26px; text-indent: 30px; margin: 5px; }
ol {
  list-style: decimal;
  padding-left: 40px;
}
    </style>
    """)
        file.close()
        webbrowser.open(name)
    def enterGeneralNote(self,text):
        if text:
            try:
                titleText = BS(self.notepad.toHtml()).get_text().split("\n")[3]
                print(titleText)
                genNote = generalNote.objects.create(noteText = text,stickyNoteParentTitle = titleText,pubDate = timezone.now())
                print("saved general note")
            except:
                genNote = generalNote.objects.create(noteText = text,stickyNoteParentTitle = "No parent")
            
    def showGeneralNotes(self):
        name = os.path.dirname(os.path.realpath(__file__)) + "\\" + "General Notes.html"
        noteFile = open(name,'w')
        noteFile.writelines("""<table style="width:100%;margin:auto 0;">""")
        noteFile.writelines("""<tr><th>Note</th><th>Date Time</th><th>Title</th></tr>""")
        for gn in generalNote.objects.order_by('stickyNoteParentTitle','-pubDate'):
            print("Parent Sticky Text:",gn.stickyNoteParentTitle)
            print("pubDate:",gn.pubDate)
            noteFile.writelines("""<tr><div><td>""" + gn.noteText + "</td><td>" + timezone.localtime(gn.pubDate,pytz.timezone("US/Central")).strftime('%B %d, %Y, %I:%M:%S %p') + "</td><td>" + gn.stickyNoteParentTitle + "</div></tr>")
        noteFile.writelines("""
                            <style>
                            th {
                             text-align:left;   
                            }
                            </style>
                            <style>
                                  @import url(http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

                            body {
                              background-color: #3e94ec;
                              font-family: "Roboto", helvetica, arial, sans-serif;
                              font-size: 16px;
                              font-weight: 400;
                              text-rendering: optimizeLegibility;
                            }

                            div.table-title {
                               display: block;
                              margin: auto;
                              max-width: 600px;
                              padding:5px;
                              width: 100%;
                            }

                            .table-title h3 {
                               color: #fafafa;
                               font-size: 30px;
                               font-weight: 400;
                               font-style:normal;
                               font-family: "Roboto", helvetica, arial, sans-serif;
                               text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
                               text-transform:uppercase;
                            }


                            /*** Table Styles **/

                            .table-fill {
                              background: white;
                              border-radius:3px;
                              border-collapse: collapse;
                              height: 320px;
                              margin: auto;
                              max-width: 600px;
                              padding:5px;
                              width: 100%;
                              box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
                              animation: float 5s infinite;
                            }
                             
                            th {
                              color:#D5DDE5;;
                              background:#1b1e24;
                              border-bottom:4px solid #9ea7af;
                              border-right: 1px solid #343a45;
                              font-size:23px;
                              font-weight: 100;
                              padding:24px;
                              text-align:left;
                              text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                              vertical-align:middle;
                            }

                            th:first-child {
                              border-top-left-radius:3px;
                            }
                             
                            th:last-child {
                              border-top-right-radius:3px;
                              border-right:none;
                            }
                              
                            tr {
                              border-top: 1px solid #C1C3D1;
                              border-bottom-: 1px solid #C1C3D1;
                              color:#666B85;
                              font-size:16px;
                              font-weight:normal;
                              text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
                            }
                             
                            tr:hover td {
                              background:#4E5066;
                              color:#FFFFFF;
                              border-top: 1px solid #22262e;
                              border-bottom: 1px solid #22262e;
                            }
                             
                            tr:first-child {
                              border-top:none;
                            }

                            tr:last-child {
                              border-bottom:none;
                            }
                             
                            tr:nth-child(odd) td {
                              background:#EBEBEB;
                            }
                             
                            tr:nth-child(odd):hover td {
                              background:#4E5066;
                            }

                            tr:last-child td:first-child {
                              border-bottom-left-radius:3px;
                            }
                             
                            tr:last-child td:last-child {
                              border-bottom-right-radius:3px;
                            }
                             
                            td {
                              background:#FFFFFF;
                              padding:20px;
                              text-align:left;
                              vertical-align:middle;
                              font-weight:300;
                              font-size:18px;
                              text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
                              border-right: 1px solid #C1C3D1;
                            }

                            td:last-child {
                              border-right: 0px;
                            }

                            th.text-left {
                              text-align: left;
                            }

                            th.text-center {
                              text-align: center;
                            }

                            th.text-right {
                              text-align: right;
                            }

                            td.text-left {
                              text-align: left;
                            }

                            td.text-center {
                              text-align: center;
                            }

                            td.text-right {
                              text-align: right;
                            }

                                </style>
                            """)
        noteFile.close()
        webbrowser.open(name)
