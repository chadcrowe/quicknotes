from django.db import models
from tkinter import ttk
from django.utils import timezone
import django

# Create your models here.
class OldNoteEntry(models.Model):
    noteText = models.CharField(max_length=1000)
    pubDate = models.DateTimeField('date published',default = django.utils.timezone.now)
    stickyNoteParent = models.ForeignKey('OldStickyNote.OldStickyNoteInstance',null = True)
