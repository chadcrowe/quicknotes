from django.apps import AppConfig


class OldchecklistConfig(AppConfig):
    name = 'OldChecklist'
