from django.db import models
import uuid
from django.utils import timezone
import django
class generalNote(models.Model):
    pubDate = models.DateTimeField('date published',default = django.utils.timezone.now)
    unique_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    noteText = models.CharField(max_length=1000)
    stickyNoteParentTitle =  models.CharField(max_length=1000)

    
